//
//  BMI.swift
//  BMI-Calculator
//
//  Created by Marcin Kępa on 11/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

import UIKit

struct BMI {
    let value: Float
    let advice: String
    let color: UIColor
}
