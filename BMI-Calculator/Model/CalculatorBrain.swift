//
//  CalculatorBrain.swift
//  BMI-Calculator
//
//  Created by Marcin Kępa on 11/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

import UIKit

struct CalculatorBrain {
    
    var bmi: BMI?
    var bmiValue: Float
    
    mutating func calculateBMI(height: Float, weight: Float) {
        bmiValue = weight / (height*height)
        bmi = BMI(value: bmiValue, advice: bmiAdvide(), color: bmiColor())
    }
    
    func getBMIValue() -> String{
        return String(format: "%.1f", bmi?.value ?? 0.0)
    }
    func bmiAdvide() -> String{
        if bmiValue < 18.5{
            return "You should eat more!"
        }else if bmiValue < 24.9 {
            return "You are healthy"
        }else{
            return "You should eat less!"
        }
    }
    func bmiColor() -> UIColor {
        if bmiValue < 18.5{
            return #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        }else if bmiValue < 24.9 {
            return #colorLiteral(red: 0.7097135581, green: 1, blue: 0.6672230565, alpha: 1)
        }else{
            return #colorLiteral(red: 1, green: 0.5100439158, blue: 0.4049531939, alpha: 1)
        }
    }
}
