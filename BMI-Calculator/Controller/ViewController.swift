//
//  ViewController.swift
//  BMI-Calculator
//
//  Created by Marcin Kępa on 10/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

import UIKit

class CalculateViewController: UIViewController {

    @IBOutlet weak var weightSliderOutlet: UISlider!
    @IBOutlet weak var highSliderOutlet: UISlider!
    @IBAction func heightSlider(_ sender: UISlider) {
        heightLabel.text = (String(format: "%.2f m", sender.value))
    }
    
    @IBAction func weightSlider(_ sender: UISlider) {
        weightLabel.text = (String(format: "%.0f kg", sender.value))
    }
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    @IBAction func calculatePressed(_ sender: UIButton) {
        let weight = weightSliderOutlet.value
        let height = highSliderOutlet.value
        let BMI = weight / (height*height)
        
        let secondVC = SecondViewController()
        secondVC.bmiValue = String(format: "%.1f", BMI)
        self.present(secondVC, animated: true, completion: nil)
    }
    
}

